Formbuilder::Application.routes.draw do

  #root to: 'home#show'
  root to:'forms#index'

  #resources :forms
  get 'forms/:id/fields' => 'forms#fields', :as => 'edit_form_fields'
  put 'forms/:id/fields' => 'forms#save_fields', :as => 'update_form_fields'

  #get 'forms/:form_id/responses/:id/thanks' => 'responses#thanks', :as => 'responses_thanks'
  get 'forms/:form_id/responses/thanks' => 'responses#thanks', :as => 'responses_thanks'

  resources :forms do
    #responses don't really need to be edited so no put/patch
       resources :responses

   end
end
