//a simple sample to build on...
/*
(function($) {

    $.fn.helloWorld = function() {

        this.each( function() {
            $(this).text("Hello, World!");
        });

    }

}(jQuery));*/





(function($){
    jsonEndPoint = "";
    responseEndPoint = "";



    //we'll obviously need this in the future, but now sure about right now...
    //var settings = $.extend({})

    //for accessbility wrap any text in a label....

    function radioField(fieldData){
        //console.log(JSON.stringify(fieldData));
        var questionContainer = $("<div></div>", {id:fieldData.id, class:"response-field-radio"});
        var questionLabel = $("<label/>").append($("<span></span>", {text:fieldData.label, class:"questionLabel"}));
        $(questionContainer).append(questionLabel)

        var radioOptions = fieldData.field_options.options;

        for(r in radioOptions){
            var questionOptionContainer = $("<div></div>");
            var questionOptionLabel = $("<label></label>", {class:"fb-option"});//, text:radioOptions[r].label});
            var questionOptionRadioButton = $("<input>", {type:"radio", name:fieldData.cid+'_'+radioOptions[r].label.replace(/ /g, "_").toLowerCase()});//, checked:radioOptions[r].checked});
            //var questionOptionRadioButton = $("<input>", {type:"radio", name:radioOptions[r].label});//, checked:radioOptions[r].checked});
            //now assemble the parts...

            $(questionOptionRadioButton).appendTo(questionOptionLabel);
            $(questionOptionLabel).append(radioOptions[r].label);
            $(questionOptionLabel).appendTo(questionOptionContainer);
            $(questionOptionContainer).appendTo(questionContainer);
        }
        if(fieldData.field_options.include_other_option){

            var questionOptionContainer = $("<div></div>");
            var questionOptionLabel = $("<label></label>", {class:"fb-option"});//, text:radioOptions[r].label});
            var questionOptionRadioButton = $("<input>", {type:"radio", name:fieldData.cid+"_other"});//, checked:radioOptions[r].checked});
            var questionOptionTextBox = $("<input>", {type:"text", name:fieldData.cid+"_other_text"});
            questionOptionTextBox.css({"display":"inline-block", "marginLeft":"5px"});
            //now assemble the parts...



            $(questionOptionRadioButton).appendTo(questionOptionLabel);
            $(questionOptionLabel).append("Other");
            $(questionOptionLabel).appendTo(questionOptionContainer);
            $(questionOptionTextBox).appendTo(questionOptionLabel);
            $(questionOptionContainer).appendTo(questionContainer);

        }
        return questionContainer;
    }

    function textField(fieldData){
        //console.log(JSON.stringify(fieldData));
        var questionContainer = $("<div></div>", {id:fieldData.id, class:"response-field-text"});
        var questionFieldSet = $("<fieldset/>");

        var questionLabel = $("<label/>").append($("<span></span>", {text:fieldData.label, class:"questionLabel"}));

        $(questionFieldSet).append(questionLabel);
        //formbuilder css uses small medium large as an option here...but for responsive stuff that seems unnecessary

        //var textOptions = fieldData.field_options;
        //fb does min, max in words, or characters.  this seems like it should be part of validation which I'll put in later.
        var textInput = $("<input>", {type:"text", name:fieldData.cid});
        var textDescription = $("<span></span>", {text:fieldData.field_options.description, class:"questionDescription"});


        $(questionFieldSet).append(textInput);
        $(questionFieldSet).append(textDescription);
        $(questionContainer).append(questionFieldSet)

        return questionContainer;
    }

    function checkBox(fieldData){
        //console.log(JSON.stringify(fieldData));
        var questionContainer = $("<div></div>", {id:fieldData.id, class:"response-field-checkboxes"});
        var questionLabel = $("<label/>").append($("<span></span>", {text:fieldData.label, class:"questionLabel"}));
        $(questionContainer).append(questionLabel);

        var checkBoxOptions = fieldData.field_options.options;

        for (c in checkBoxOptions){
            var questionOptionContainer = $("<div></div>");
            var questionOptionLabel = $("<label></label>", {class:"fb-option"});//, text:checkBoxOptions[c].label});
            var questionOptionCheckBox = $("<input>", {type:"checkbox", name:fieldData.cid+"_"+checkBoxOptions[c].label.replace(/ /g, "_").toLowerCase()});

            $(questionOptionCheckBox).appendTo(questionOptionLabel);

            $(questionOptionLabel).append(checkBoxOptions[c].label);

            $(questionOptionLabel).appendTo(questionOptionContainer);

            $(questionOptionContainer).appendTo(questionContainer);

        }
        //console.log(fieldData.field_options.include_other_option);
        if(fieldData.field_options.include_other_option){
            var questionOptionContainer = $("<div></div>");
            var questionOptionLabel = $("<label></label>", {class:"fb-option"});
            var questionOptionCheckBox = $("<input>", {type:"checkbox", name:fieldData.cid+"_other"});
            var questionOptionTextBox = $("<input>", {type:"text", name:fieldData.cid+"_other_text"});
            questionOptionTextBox.css({"display":"inline-block", "marginLeft":"5px"});



            $(questionOptionCheckBox).appendTo(questionOptionLabel);
            $(questionOptionLabel).append("Other");
            $(questionOptionLabel).appendTo(questionOptionContainer);
            $(questionOptionTextBox).appendTo(questionOptionLabel);
            $(questionOptionContainer).appendTo(questionContainer);
        }

        var questionDescription = $("<span></span>", {text:fieldData.field_options.description, class:"questionDescription"});
        $(questionContainer).append(questionDescription);

        return questionContainer;

    }

    function dateField(fieldData){
        //console.log(JSON.stringify(fieldData));
        var questionContainer = $("<div></div>", {id:fieldData.id, class:"response-field-date"});
        var questionFieldSet = $("<fieldset/>");
        var questionLabel = $("<label/>").append($("<span></span>", {text:fieldData.label, class:"questionLabel"}));
        $(questionFieldSet).append(questionLabel);

        var inputLine = $('<div class="input-line"><span class="month"><input name="'+fieldData.cid+'_month" type="text"><label>MM</label></span><span class="above-line">/</span><span class="day"><input type="text" name="'+fieldData.cid+'_day"><label>DD</label></span><span class="above-line">/</span><span class="year"><input type="text" name="'+fieldData.cid+'_year"><label>YYYY</label></span></div>', {class:"input-line"});


        $(questionFieldSet).append(inputLine);

        var questionDescription = $("<span></span>", {text:fieldData.field_options.description, class:"questionDescription"});
        $(questionFieldSet).append(questionDescription);
        $(questionContainer).append(questionFieldSet);

        return questionContainer;

    }

    function timeField(fieldData){
        //console.log(JSON.stringify(fieldData));
        var questionContainer = $("<div></div>", {id:fieldData.id, class:"response-field-time"});
        var questionLabel = $("<label/>").append($("<span></span>", {text:fieldData.label, class:"questionLabel"}));
        $(questionContainer).append(questionLabel);

        var inputLine = $('<div class="input-line"><span class="hours"><input type="text" name="'+fieldData.cid+'_hour"><label>HH</label></span><span class="above-line">:</span><span class="minutes"><input type="text" name="'+fieldData.cid+'_minutes"><label>MM</label></span><span class="above-line">:</span><span class="seconds"><input type="text" name="'+fieldData.cid+'_seconds"><label>SS</label></span><span class="am_pm"><select name="'+fieldData.cid+'_ampm"><option value="am">AM</option><option value="pm">PM</option></select></span></div>');

        $(questionContainer).append(inputLine);

        var questionDescription = $("<span></span>", {text:fieldData.field_options.description, class:"questionDescription"});
        $(questionContainer).append(questionDescription);


        return questionContainer;

    }

    function websiteField(fieldData){
        //console.log(JSON.stringify(fieldData));
        var questionContainer = $("<div></div>", {id:fieldData.id, class:"response-field-website"});
        var questionLabel = $("<label/>").append($("<span></span>", {text:fieldData.label, class:"questionLabel"}));
        $(questionContainer).append(questionLabel);

        var inputLine = $('<input type="text" placeholder="http://" name="'+fieldData.cid+'">  <span class="help-block">  </span>');

        $(questionContainer).append(inputLine);

        var questionDescription = $("<span></span>", {text:fieldData.field_options.description, class:"questionDescription"});
        $(questionContainer).append(questionDescription);

        return questionContainer;
    }

    function priceField(fieldData){
        var questionContainer = $("<div></div>", {id:fieldData.id, class:"response-field-price"});
        var questionLabel = $("<label/>").append($("<span></span>", {text:fieldData.label, class:"questionLabel"}));
        $(questionContainer).append(questionLabel);

        var inputLine = $('<div class="input-line"><span class="above-line">$</span><span class="dolars"><input type="text" name="'+fieldData.cid+'_dolars"></span><span class="above-line">.</span><span class="cents"><input type="text" name="'+fieldData.cid+'_cents"></span></div>');

        $(questionContainer).append(inputLine);

        var questionDescription = $("<span></span>", {text:fieldData.field_options.description, class:"questionDescription"});
        $(questionContainer).append(questionDescription);

        return questionContainer;

    }

    function fileField(fieldData){
        var questionContainer = $("<div></div>", {id:fieldData.id, class:"response-field-price"});
        var questionLabel = $("<label/>").append($("<span></span>", {text:fieldData.label, class:"questionLabel"}));
        $(questionContainer).append(questionLabel);

        var inputLine = $("<input>", {type:"file", name:fieldData.cid});

        $(questionContainer).append(inputLine);
        var questionDescription = $("<span></span>", {text:fieldData.field_options.description, class:"questionDescription"});
        $(questionContainer).append(questionDescription);

        return questionContainer;
    }

    function dropDownField(fieldData){
        //console.log(JSON.stringify(fieldData));
        var questionContainer = $("<div></div>", {id:fieldData.id, class:"response-field-price"});
        var questionLabel = $("<label/>").append($("<span></span>", {text:fieldData.label, class:"questionLabel"}));
        $(questionContainer).append(questionLabel);

        var selectOptions = fieldData.field_options.options;

        var select = $("<select></select>", {name:fieldData.cid});

        for (o in selectOptions){

            if(selectOptions[o].checked == "true"){

                var option = $("<option selected></option>", {text:selectOptions[o].label});
                $(select).append(option);
            }else{
                var option = $("<option></option>", {text:selectOptions[o].label});
                $(select).append(option);
            }
        }

        $(questionContainer).append(select);

        var questionDescription = $("<span></span>", {text:fieldData.field_options.description, class:"questionDescription"});
        $(questionContainer).append(questionDescription);

        return questionContainer;

    }

    function addressField(fieldData){
        //console.log(JSON.stringify(fieldData));
        var questionContainer = $("<div></div>", {id:fieldData.id, class:"response-field-website"});
        var questionLabel = $("<label/>").append($("<span></span>", {text:fieldData.label, class:"questionLabel"}));
        $(questionContainer).append(questionLabel);

        var addressInput = $('<div class="input-line"><span class="street"><label>Address</label><input type="text" name="'+fieldData.cid+'_street"></span></div><div class="input-line"><span class="city"><label>City</label><input type="text" name="'+fieldData.cid+'_city"></span><span class="state"><label>State / Province / Region</label><input type="text" name="'+fieldData.cid+'_region"></span></div><div class="input-line"><span class="zip"><label>Zipcode</label><input type="text" name="'+fieldData.cid+'_zipcode"></span><span class="country"><label>Country</label><select name="'+fieldData.cid+'_country"><option>United States</option></select></span></div>');
        
        $(questionContainer).append(addressInput);
        


        var questionDescription = $("<span></span>", {text:fieldData.field_options.description, class:"questionDescription"});
        $(questionContainer).append(questionDescription);

        return questionContainer;
    }


    function constructFormFromData(fd, target){

        //console.log(JSON.stringify(fd));

        var title = fd.name;
        var description = fd.description;
        var fields = fd.form_fields;

        $(target).append("<h1>"+title+"</h1>");
        $(target).append("<h5>"+description+"</h5>");

        var root;

        //if target is not a form element, make a form and append it, using the form as the root element for construction.
        if($(target).nodeName != "form"){
            root = $("<form></form>", {id:fd.id, method:"POST", action:responseEndPoint, target:""});
            $(target).append(root);
        }else{
            root = target;
            root.attr("id", fd.id);
        }
        //else use the target (which is a form) as the root element.
        

        for (f in fields ){
            //console.log(fields[f].field_type);

            switch(fields[f].field_type){

                case "text":
                    $(root).append(textField(fields[f]));
                    break;
                case "checkboxes":
                    $(root).append(checkBox(fields[f]));
                    break;
                case "date":
                    $(root).append(dateField(fields[f]));
                    break;
                case "time":
                    $(root).append(timeField(fields[f]));
                    break;
                case "website":
                    $(root).append(websiteField(fields[f]));
                    break;
                case "price":
                    $(root).append(priceField(fields[f]));
                    break;
                case "file":
                    $(root).append(fileField(fields[f]));
                    break;
                case "paragraph":
                    //
                    break;
                case "radio":
                    $(root).append(radioField(fields[f]));
                    break;
                case "dropdown":
                    $(root).append(dropDownField(fields[f]));
                    break;
                case "number":
                    //
                    break;
                case "email":
                    //
                    break;
                case "address":
                    $(root).append(addressField(fields[f]));
                    break;
                case "section_break":
                    //
                    break;

            }

        }


        submitButton = $("<input>", {type:"submit"});

        $(root).append(submitButton);
        //for each question, make sure to assign the "name" to the CID.

        //probably don't want serialize, we'll want to do a post instead.....


        //use the URL passed in at init to figure out the end point to post to. also, code the end point.

        $(root).on("submit", function(e){
            /*console.log("Im posting?");
            console.log(e);
            e.preventDefault();*/



        });




    }


    $.fn.emmbedFormBuilderFormFromUrl= function(url){

        //console.log(url);


        if(url.indexOf(".json?callback=") != -1){
            jsonEndPoint = url;
            responseEndPoint  = url.replace(".json?callback=?", "/response");
        }else{
            //then its not already part of the url...
            jsonEndPoint = url+".json?callback=?";
            responseEndPoint = url+"/responses";

        }

        console.log("jsonEndPoinst: "+jsonEndPoint+", responseEndPoint :"+responseEndPoint);

        target = this;
        $.getJSON(jsonEndPoint, function(data){
            //well need an actual form...

            constructFormFromData(data, target);

        });


    }

}(Zepto));