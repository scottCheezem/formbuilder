class FormsController < ApplicationController

  # GET /forms
  def index
    @forms = Form.all
  end

  # GET /forms/1
  def show

    #@form = Form.find(params[:id])
    @form = Form.findByCode(params[:id])

    respond_to do |format|

      if params[:callback]
        format.json { render :json => @form.render_json, :callback => params[:callback] }
      else
        format.json { render :json => @form.render_json }
      end
    format.html { }

    end
  end

  # GET /forms/new
  def new
    @form = Form.new
  end


=begin
  # GET /forms/1/edit
  def edit

    #@form = Form.find(params[:id])
    @form = Form.findByCode(params[:id])

  end
=end

  # POST /forms
  def create
    @form = Form.new(form_params)

    if @form.save
      #redirect_to edit_form_fields_path(@form), notice: 'Form was successfully created.'
      render action: 'edit' #does this ever get called?
    else
      render action: 'new'
    end
  end

  def update
    @form = Form.findByCode(params[:id])
    #@form = Form.find(params[:id])

    if @form.update_attributes(form_params)
      redirect_to forms_path, notice: 'Form was successfully updated.'
    else
      render action: 'edit'
    end

  end

  #def fields
  def edit
    #@form = Form.find(params[:id])
    @form = Form.findByCode(params[:id])
    @paste_code = paste_code(request.original_url)

  end

  def save_fields

    #@form = Form.find(params[:id])
    @form = Form.findByCode(params[:id])

    form_fields = params[:fields] ||= []
    valid_cid_list = []

    count = 0
    unless form_fields.empty?
      form_fields.each do |i, field|

        valid_cid_list.push(field[:cid])
        field[:ordinality] = count

        ff = @form.form_fields.find_by(:cid => field[:cid])

        if ff.nil?
          ff = @form.form_fields.build
        end

        ff.update_attributes(field)

        count = count+1

      end
    end

    # ignore currently set fields
    if valid_cid_list.length > 0
      @form.form_fields.where("form_fields.cid NOT IN (?)", valid_cid_list).delete_all
    else
      @form.form_fields.delete_all
    end

    respond_to do |format|
      format.json { render :json => @form.render_json }
    end

  end


  # DELETE /forms/1
  def destroy
    @form = Form.findByCode(params[:id])
    #@form = Form.find(params[:id])

    @form.destroy
    redirect_to forms_url, notice: 'Form config was successfully destroyed.'
  end

  private

  # Only allow a trusted parameter "white list" through.
  def form_params
    params.require(:form).permit(:name, :description, :active, :deleted, :response_id)
  end


#ugh...this kind of breaks my heart that this is here and not in the model...
  def paste_code(url)
    return '<html>
      <head>
      <link rel="stylesheet" type="text/css" href="informer.css">
      <link rel="stylesheet" type="text/css" href="../formbuilder/formbuilder.css">
    <script type="text/javascript">
        function staticLoadScript(url)
        {
            document.write(\'<script src="\', url, \'" type="text/JavaScript"><\/script>\');
        }
        if (typeof jQuery == \'undefined\'){ //Load the jQuery Lib, only if it is not already loaded by the browser in this scope.                                                                                                                                                                                                                        staticLoadScript("http://code.jquery.com/jquery-latest.min.js");
    }
    </script>
    <script type="text/javascript" src="informer.js"></script>
    <script type="text/javascript">
        $(document).ready(function(){
            $("#formHolder").emmbedFormBuilderFormFromUrl("'+"#{url}"+'.json?callback=?");
    });
    </script>
</head>
    <body>
    <div id="formHolder">
    </div>
    </body>
    </html>'
    #.gsub("<", "&lt;").gsub(">", "&gt;")

  end


end
