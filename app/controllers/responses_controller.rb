class ResponsesController < ApplicationController

  before_action :load_form

  before_action :set_responses, only: [:show, :edit, :update, :destroy]


  def index
    @responses = @form.responses

    respond_to do |format|


      if params[:callback]
        format.json{render :json => @form.render_responses_json, :callback => params[:callback] }
      else
        format.json{render json: @form.render_responses_json}
      end
      format.html{}
    end

  end

  def show
       #@response = @form.responses.find(params[:id])

        #render :json => @responses.render_json

  end

  def new
    @responses = @form.responses.new
  end


  #def edit
    #@response = @form.responses.find(params[:id]) #this should be taken care of by set_response...
  #end

  def create
    @response = @form.responses.new




    respond_to do |format|
      if @response.save  # maybe redirect to a thanks url, with the referer page passed in a param? old school

        params.each do |key, val|
          if( key =~ /^c[0-9]+/ )
            puts "#{key} : #{val}"
            @response.field_responses.new(:key => key, :value => val).save

          end


        end



        #format.html { render :json => params }
        format.html { redirect_to action:"thanks"}
        format.json {} #probably not going to happen any time soon, unless we release a framework for
      else
        format.html { redirect_to action:"forms#index"}
        format.json {render json: @response.errors, status: :unprocessable_entity}
      end

    end

  end


  def thanks
    render layout:false
  end


  def update
    # for responses, there might not really be a need for an update....
  end

  def destroy
    #or to destroy....except perhaps by form creators...
  end

  private

  def set_responses
    @responses = @form.responses.find(params[:id]);
  end

  #don't trust params fromt he scary internets
  def response_params
    params.require(:response).permit(:form_id, :id, )
  end

  def load_form
    @form = Form.where("urlref = ? ", params[:form_id]).first
  end




end
