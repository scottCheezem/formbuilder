class FormField < ActiveRecord::Base

  before_save :normalize_data

  serialize :field_options

  belongs_to :form

  default_scope order(:ordinality => :asc)

  private

  def normalize_data


    unless self.field_options.blank? || self.field_options[:options].blank?
      options = []

      self.field_options[:options].each do |i, option|
        options.push(option)
      end

      self.field_options[:options] = options

    end

  end

end
