class Response < ActiveRecord::Base


  belongs_to  :form
  has_many :field_responses


  def render_json
    self.to_json({:include => [:field_responses]}).html_safe
  end



end
