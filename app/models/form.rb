class Form < ActiveRecord::Base



  validates :name, :presence => true

  has_many :form_fields, :dependent => :destroy

  has_many :responses, :dependent => :destroy

  after_create :postProcess

  def render_json

    self.to_json({:include => [:form_fields]}).html_safe

  end


  def render_responses_json
    self.responses.as_json(:include => [:field_responses])
  end

  def to_param
    urlref
  end

  def self.findByCode(r)
    where({:urlref => r}).first
  end



  private

  def postProcess
    self.urlref = SecureRandom.hex(8)

    self.save
  end


end
