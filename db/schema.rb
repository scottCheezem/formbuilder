# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20140221172050) do

  create_table "field_responses", force: true do |t|
    t.integer  "response_id"
    t.string   "value"
    t.string   "key"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "field_responses", ["response_id"], name: "index_field_responses_on_response_id"

  create_table "form_fields", force: true do |t|
    t.string   "label"
    t.string   "field_type"
    t.boolean  "required"
    t.text     "field_options"
    t.string   "cid"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "form_id"
    t.boolean  "admin_only",    default: false
    t.integer  "ordinality",    default: 0
  end

  create_table "forms", force: true do |t|
    t.string   "name"
    t.text     "description"
    t.boolean  "active",      default: true
    t.datetime "created_at"
    t.datetime "updated_at"
    t.boolean  "deleted",     default: false
    t.string   "urlref"
  end

  create_table "responses", force: true do |t|
    t.integer  "form_id"
    t.string   "uuid"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "responses", ["form_id"], name: "index_responses_on_form_id"

end
