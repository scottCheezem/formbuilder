class CreateFieldResponses < ActiveRecord::Migration
  def change

    create_table :field_responses do |t|
      t.references :response, index: true
      t.string :value
      t.string :key
      t.timestamps
    end
  end

end
