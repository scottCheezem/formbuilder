class CreateResponses < ActiveRecord::Migration
  def change

    create_table :responses do |t|
      t.references :form, index: true
      #t.references :field_response, index:true
      #t.references :user
      t.string :uuid
      t.timestamps
    end

  end
end
