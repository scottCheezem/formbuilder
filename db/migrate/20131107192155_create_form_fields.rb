class CreateFormFields < ActiveRecord::Migration
  def change
    create_table :form_fields do |t|
      t.string :label
      t.string :field_type
      t.boolean :required
      t.text :field_options
      t.string :cid

      t.timestamps
    end
  end
end
