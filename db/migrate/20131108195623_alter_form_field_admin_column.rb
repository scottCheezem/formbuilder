class AlterFormFieldAdminColumn < ActiveRecord::Migration
  def self.up
    add_column :form_fields, :admin_only, :boolean, :default => false
    add_column :form_fields, :ordinality, :integer, :default => 0
  end

  def self.down
    remove_column :form_fields, :admin_only
    remove_column :form_fields, :ordinality
  end
end
