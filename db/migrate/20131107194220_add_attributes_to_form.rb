class AddAttributesToForm < ActiveRecord::Migration
  def self.up
    change_column_default :forms, :active, true
    add_column :forms, :deleted,  :boolean, :default => false
  end

  def self.down
    change_column_default :forms, :active, nil
    remove_column :forms, :deleted
  end
end
